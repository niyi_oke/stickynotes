package stickynotes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import stickynotes.StickyNotes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BigBen
 */
public class Sticker extends Stage {

    private final TextArea textArea;

    private final Button sendToButton;

    private final Button newButton;

    private final Button closeButton;

    public Sticker() {

        textArea = new TextArea();
        textArea.setPromptText("Write Here");
        textArea.setStyle("-fx-text-fill: black; -fx-highlight-fill: yellow");

        closeButton = new Button("x");
        closeButton.setOnAction(new CloseButtonEvent());
        closeButton.setStyle("-fx-background-color: tomato; -fx-text-fill: black");

        newButton = new Button("+");
        newButton.setOnAction(new NewPadEvent());
        newButton.setAlignment(Pos.TOP_LEFT);
        newButton.setStyle("-fx-background-color: tomato; -fx-text-fill: black");

        sendToButton = new Button("==>");
        sendToButton.setOnAction(new SendToButtonEvent());
        sendToButton.setAlignment(Pos.TOP_RIGHT);
        sendToButton.setStyle("-fx-background-color: tomato; -fx-text-fill: black");

        BorderPane borderPane = new BorderPane();
        borderPane.setStyle("-fx-background: tomato");
        borderPane.setLeft(newButton);
        borderPane.setRight(sendToButton);

        BorderPane root = new BorderPane();
        root.setTop(borderPane);
        root.setCenter(textArea);

        root.setStyle("-fx-background-color: tomato;");
        
        HBox hb = new HBox(closeButton);
        hb.setAlignment(Pos.TOP_RIGHT);
        hb.setStyle("-fx-background-color: tomato");
        
        VBox vb = new VBox(hb, root);
        Scene scene = new Scene(vb, 300, 250);

        scene.getStylesheets().add("stickynotes/textarea.css");

        initStyle(StageStyle.TRANSPARENT);
        setTitle("Sticky");
        setScene(scene);
    }

    public class SendToButtonEvent implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            System.out.println("SendToButton Event");
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save As");
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().add(extFilter);

            File file = fileChooser.showSaveDialog(((Button) event.getSource()).getScene().getWindow());

            if (Objects.nonNull(file)) {
                FileWriter fileWriter = null;
                try {
                    String text = textArea.getText();
                    fileWriter = new FileWriter(file);
                    fileWriter.write(text);
                    fileWriter.close();
                } catch (IOException ex) {
                    Logger.getLogger(StickyNotes.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("IOException in SendToButtonEvent method");
                }
            }

        }

    }

    public class NewPadEvent implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Button source = (Button) event.getSource();
                    Window window = source.getScene().getWindow();
                    double x = window.getX() + 20;
                    double y = window.getY() + 20;
                    Sticker sticker = new Sticker();
                    sticker.setX(x);
                    sticker.setY(y);
                    sticker.show();

                }
            });
        }

    }

    public class CloseButtonEvent implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Button button = (Button) event.getSource();
                    Window window = button.getScene().getWindow();
                    window.hide();
                }
            });
        
        }

    }
}